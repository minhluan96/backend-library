var express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    jwt = require('jsonwebtoken'),
    cors = require('cors');

var userController = require('./controllers/userController');
var bookController = require('./controllers/bookController');
var typeController = require('./controllers/typeController');
var memberCardController = require('./controllers/memberCardController');
var ticketController = require('./controllers/ticketController');
var configureController = require('./controllers/configureController');


var app = express();

app.use(morgan('dev'));
app.use(bodyParser('json'));
app.use(cors())

app.get ('/', (req, res) => {
  res.json({
    msg: 'Backend library management'
  });
})

var verifyAccessToken = (req, res, next) => {
  var token = req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, 'SECRET-LIBRARY', (err, payload) => {
      if (err) {
        res.statusCode = 403;
        res.json({
          msg: 'INVALID TOKEN',
          error: err
        });
      } else {
        req.token_payload = payload;
        next();
      }
    })
  } else {
    res.statusCode = 403;
    res.json({
      msg: `TOKEN DOESN'T EXIST`
    });
  }
}

app.use('/users', userController);
app.use('/types', verifyAccessToken, typeController);
app.use('/books', verifyAccessToken, bookController);
app.use('/memberCards', verifyAccessToken, memberCardController);
app.use('/tickets', verifyAccessToken, ticketController);
app.use('/config', verifyAccessToken, configureController);


app.use((req, res, next) => {
  var err = new Error("Not found");
  err.status = 404;
  next(err);
})

app.use((err, req, res, next) => {
  console.log(err)
  res.status(err.status || 500);
  res.json({
    'error': err.message
  });
})

var port = process.env.port || 3000;
app.listen(port, () => {
  console.log(`Server is running on ${port}`);
})

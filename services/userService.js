var md5 = require('crypto-js/md5');
var db = require('../utils/mysql-provider');

exports.login = loginEntity => {
  var md5_pw = md5(loginEntity.password);
  var sql = `select * from THU_THU where TenTaiKhoan = '${loginEntity.username}' and MatKhau = '${md5_pw}'`;
  return db.load(sql);
}

exports.createAccountManager = function(userEntity) {
  var md5_pw = md5(userEntity.password);
  var refreshToken = md5(Date.now());

  return new Promise((resolve, reject) => {
    var sql = `select * from THU_THU where TenTaiKhoan = '${userEntity.username}'`
    db.load(sql).then(value => {
      if (value.length > 0) {
        var error = new Error('Tên tài khoản đã tồn tại. Vui lòng chọn tên khác')
        reject(error)
      } else {
        var query = `insert into THU_THU(ID, TenTaiKhoan, MatKhau, RefreshToken, isQuanLy) values(${userEntity.id}, '${userEntity.username}', '${md5_pw}', '${refreshToken}', ${userEntity.isQuanLy})`
        return db.write(query)
      }
    }).then(value => {
      resolve(value)
    }).catch(err => {
      reject(err)
    })
  })
}

exports.createAccountUser = function(userEntity) {
  var sql = `insert into NGUOI_DUNG(HoTen, NgaySinh, Email, TinhTrang) values ('${userEntity.HoTen}', '${userEntity.NgaySinh}', '${userEntity.Email}', '${userEntity.TinhTrang}')`
  return db.write(sql);
}

exports.checkAccountExist = function(userEntity) {
  var sql = `select * from THU_THU where TenTaiKhoan = ${userEntity.username}`
  return db.load(sql)
}

exports.getTotalNormalUser = function() {
  var sql = `select count(*) as total from NGUOI_DUNG nd join DOC_GIA dg on nd.ID = dg.ID`
  return db.load(sql)
}

exports.getNormalUserPerPage = function (limit, offset) {
  var sql = `select nd.*, dg.DiaChi from NGUOI_DUNG nd join DOC_GIA dg on nd.ID = dg.ID limit ${limit} offset ${offset}`
  console.log(sql)
  return db.load(sql)
}

exports.getTotalAdminUser = function () {
  var sql = `select count(*) as total from NGUOI_DUNG nd join THU_THU tt on nd.ID = tt.ID`
  return db.load(sql)
}

exports.getAdminUserPerPage = function (limit, offset) {
  var sql = `select nd.*, tt.TenTaiKhoan, tt.MatKhau, tt.isQuanLy as CapBac from NGUOI_DUNG nd join THU_THU tt on nd.ID = tt.ID limit ${limit} offset ${offset}`
  return db.load(sql)
}

exports.createNormalUser = function (userEntity) {
  console.log(userEntity)
  var sql = `insert into DOC_GIA(ID, DiaChi) values (${userEntity.id}, N'${userEntity.DiaChi}')`
  return db.write(sql)
}

exports.updateNormalUser = function (userEntity) {
  var sql = `update DOC_GIA set DiaChi = N'${userEntity.DiaChi}' where ID = ${userEntity.ID}`
  return db.write(sql)
}

exports.updateUser = function (userEntity) {
  var sql = `update NGUOI_DUNG set HoTen = N'${userEntity.HoTen}', NgaySinh = '${userEntity.NgaySinh}',
            Email = '${userEntity.Email}', TinhTrang = ${userEntity.TinhTrang} where ID = ${userEntity.ID}`
  return db.write(sql)
}

exports.updateAdminUser = function (userEntity) {
  var query = ""
  if (userEntity.password !== '') {
    var md5_pw = md5(userEntity.password);
    query = `MatKhau = "${md5_pw}", `
  }

  var sql = `update THU_THU set ` + query + `isQuanLy = ${userEntity.isQuanLy} where ID = ${userEntity.id}`
  return db.write(sql)
}

exports.deleteUser = function (userEntity) {
  var sql = `update NGUOI_DUNG set TinhTrang = 0 where ID = ${userEntity.ID}`
  return db.write(sql)
}

exports.configureUser = function (userEntity) {
  var minAgeConfig = 1
  var maxAgeConfig = 2
  var sql = `insert into CHI_TIET_CAU_HINH_DOC_GIA(MaDocGia, MaCauHinh, TinhTrang)
              values (${userEntity.id}, ${minAgeConfig}, 1), (${userEntity.id}, ${maxAgeConfig}, 1)`
  console.log(sql)
  return db.write(sql)
}

exports.loadAllUsers = function() {
  var sql = 'select count(*) as total from NGUOI_DUNG'
  return db.load(sql)
}

exports.getNormalIDs = function() {
  var sql = `select nd.ID from NGUOI_DUNG nd join DOC_GIA dg on nd.ID = dg.ID where nd.TinhTrang = 1 and
              nd.ID not in (select tdg.MaDocGia from THE_DOC_GIA tdg where tdg.MaDocGia = nd.ID)`
  return db.load(sql)
}

exports.getNormalUserIdsActive = function () {
  var sql = `select nd.ID from NGUOI_DUNG nd join DOC_GIA dg on nd.ID = dg.ID where nd.TinhTrang = 1`
  return db.load(sql)
}

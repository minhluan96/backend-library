var db = require('../utils/mysql-provider');

exports.getTotalMemberCards = function() {
  var sql = `select count(*) as total from THE_DOC_GIA`
  return db.load(sql)
}

exports.getMemberCardsPerPage = function (limit, offset) {
  var sql = `select tdg.*, tl.TenTheLoai as LoaiThe, nd.HoTen as TenDocGia from THE_DOC_GIA tdg join THE_LOAI_THE_DOC_GIA tl on tdg.MaTheTheLoai = tl.ID
          join NGUOI_DUNG nd on tdg.MaDocGia = nd.ID limit ${limit} offset ${offset}`
  return db.load(sql)
}

exports.updateMemberCard = function (cardEntity) {
  var sql = `update THE_DOC_GIA set NgayLapThe = '${cardEntity.NgayLapThe}', DiemTichLuy = ${cardEntity.DiemTichLuy},
            ThoiHanSuDung = '${cardEntity.ThoiHanSuDung}', MaTheTheLoai = ${cardEntity.MaTheTheLoai}, TinhTrang = ${cardEntity.TinhTrang} where ID = ${cardEntity.ID}`
  return db.write(sql)
}

exports.deleteMemberCard = function (cardEntity) {
  var sql = `update THE_DOC_GIA set TinhTrang = 0 where ID = ${cardEntity.ID}`
  return db.write(sql)
}

exports.createMemberCard = function (cardEntity) {
  var sql = `insert into THE_DOC_GIA(NgayLapThe, DiemTichLuy, ThoiHanSuDung, MaDocGia, MaTheTheLoai) values ('${cardEntity.NgayLapThe}', ${cardEntity.DiemTichLuy},
            '${cardEntity.ThoiHanSuDung}', ${cardEntity.MaDocGia}, ${cardEntity.MaTheTheLoai})`
  return db.write(sql)
}

exports.updateMemberPoint = function (increaseValue, id) {
  var sql = `select * from THE_DOC_GIA where MaDocGia = ${id}`
  return new Promise((resolve, reject) => {
    var diem = 0
    db.load(sql).then(value => {
      if (value[0] != null && value[0].DiemTichLuy != null) {
        diem = value[0].DiemTichLuy
        diem = diem + increaseValue
        var query = ''
        if (diem >= 100) {
          query = `update THE_DOC_GIA set DiemTichLuy = ${diem}, MaTheTheLoai = 1 where MaDocGia = ${id}`
        } else {
          query = `update THE_DOC_GIA set DiemTichLuy = ${diem} where MaDocGia = ${id}`
        }
        return db.write(query)
      }
    }).then(value => {
      resolve(value)
    }).catch(err => {
      reject(err)
    })
  })
}

exports.decreaseMemberPoint = function (decreaseValue, id) {
  var sql = `select * from THE_DOC_GIA where MaDocGia = ${id}`
  return new Promise((resolve, reject) => {
    var diem = 0
    db.load(sql).then(value => {
      if (value != null && value.DiemTichLuy != null) {
        diem = value.DiemTichLuy
        if (diem < decreaseValue) {
          diem = 0
        } else {
          diem = diem - decreaseValue
        }
        var query = ''
        if (diem < 100) {
          query = `update THE_DOC_GIA set DiemTichLuy = ${diem}, MaTheTheLoai = 2 where MaDocGia = ${id}`
        } else {
          query = `update THE_DOC_GIA set DiemTichLuy = ${diem} where MaDocGia = ${id}`
        }
        return db.write(query)
      }
    }).then(value => {
      resolve(value)
    }).catch(err => {
      reject(err)
    })
  })
}

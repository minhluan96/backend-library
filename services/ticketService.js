var db = require('../utils/mysql-provider');
var configureService = require('../services/configureService');

exports.loadAllTickets = function () {
  var sql = `select count(*) as total from PHIEU_MUON_TRA`
  return db.load(sql)
}

exports.loadTicketsPerPage = function (limit, offset) {
  var sql = `select p.*, nd.HoTen as TenDocGia, tl.TenPhieu as TheLoaiPhieu from PHIEU_MUON_TRA p join NGUOI_DUNG nd
              on p.MaDocGia = nd.ID join THE_LOAI_PHIEU tl on p.MaTheLoaiPhieu = tl.ID limit ${limit} offset ${offset}`
  console.log(sql)
  return db.load(sql)
}

exports.createTicket = function (ticketEntity) {
  console.log(ticketEntity)
  var sql = `insert into PHIEU_MUON_TRA(MaDocGia, NgayLapPhieu, MaTheLoaiPhieu, TinhTrang) values (${ticketEntity.MaDocGia},
              '${ticketEntity.ngayLapPhieu}', ${ticketEntity.MaTheLoaiPhieu}, 1)`
  return db.write(sql)
}

exports.createDetailBorrowBooks = function (ticketEntity) {
  var books = ticketEntity.bookList
  var query = ''
  for (var book of books) {
    if (book.isDeletedFromTicket == true) {
      continue
    }
    query += `(${book.ID}, ${ticketEntity.ID}, ${book.SoLuongMuon}), `
  }
  query = query.substring(0, query.length - 2);
  var sql = `insert into CHI_TIET_MUON_SACH(MaSach, MaPhieu, SoLuong) values ${query}`
  return db.write(sql)
}

exports.getDetailBookOfTicket = function (id) {
  var sql = `select ct.MaSach, ct.MaPhieu, ct.SoLuong as SoLuongMuon, ct.SoLuong as SoLuongMuonCu, s.ID as ID, s.TenSach as TenSach, s.SoLuong as SoLuong from CHI_TIET_MUON_SACH ct
              join SACH s on s.ID = ct.MaSach where ct.MaPhieu = ${id}`
  console.log(sql)
  return db.load(sql)
}

exports.updateBorrowTicket = function (ticketEntity) {
  var sql = `update PHIEU_MUON_TRA set NgayLapPhieu = '${ticketEntity.ngayLapPhieu}',
              TinhTrang = ${ticketEntity.TinhTrang} where ID = ${ticketEntity.ID}`
  console.log(sql)
  return db.write(sql)
}

exports.deleteAllBorrowBooks = function (id) {
  var sql = `delete from CHI_TIET_MUON_SACH where MaPhieu = ${id}`
  return db.write(sql)
}

exports.deleteTicket = function (id) {
  var sql = `update PHIEU_MUON_TRA set TinhTrang = 0 where ID = ${id}`
  return db.write(sql)
}

exports.updateDetailStatus = function(id) {
  var sql = `update CHI_TIET_MUON_SACH set DaTra = 1 where MaPhieu = ${id}`
  return new Promise((resolve, reject) => {
    db.write(sql).then(value => {
      var query = `update PHIEU_MUON_TRA set TinhTrang = 0 where ID = ${id}`
      console.log(query)
      db.write(query)
    }).then(value => {
      resolve(value)
    })
  }).catch(err => {
    reject(err)
  })
}

exports.searchTicketByTerm = function (limit, offset, term) {
  var sql = `select p.*, nd.HoTen as TenDocGia, tl.TenPhieu as TheLoaiPhieu from PHIEU_MUON_TRA p join NGUOI_DUNG nd
              on p.MaDocGia = nd.ID join THE_LOAI_PHIEU tl on p.MaTheLoaiPhieu = tl.ID
              where nd.HoTen like '%${term}%' limit ${limit} offset ${offset}`
  return db.load(sql)
}

exports.searchAll = function (term) {
  var sql = `select count(*) as total from PHIEU_MUON_TRA p join NGUOI_DUNG nd
              on p.MaDocGia = nd.ID join THE_LOAI_PHIEU tl on p.MaTheLoaiPhieu = tl.ID
              where nd.HoTen like '%${term}%'`
  return db.load(sql)
}

exports.getTotalActiveBorrowBooks = function(user_id) {
  var sql = `select sum(ct.SoLuong) as TongSachDangMuon from PHIEU_MUON_TRA p join CHI_TIET_MUON_SACH ct on p.ID = ct.MaPhieu
              where p.MaDocGia = ${user_id} and p.MaTheLoaiPhieu = 1 and p.TinhTrang = 1`
  return db.load(sql)
}

exports.getFirstTicketBorrowBook = function (user_id) {
  var sql = `select p.* from PHIEU_MUON_TRA p join CHI_TIET_MUON_SACH ct on p.ID = ct.MaPhieu
              where p.MaDocGia = ${user_id} and p.MaTheLoaiPhieu = 1 and p.TinhTrang = 1 LIMIT 1`
  console.log(sql)
  return db.load(sql)
}

exports.getAllDateOfTickets = function() {
  var sql = `select NgayLapPhieu from PHIEU_MUON_TRA group by NgayLapPhieu order by NgayLapPhieu asc limit 7`
  return db.load(sql)
}

exports.getReportOfTickets = function () {
  var sql = `select NgayLapPhieu, count(*) as total, MaTheLoaiPhieu from phieu_muon_tra group by NgayLapPhieu, MaTheLoaiPhieu order by NgayLapPhieu asc limit 7`
  return db.load(sql)
}

exports.loadBorrowTicket = function() {
  var sql = `select pmt.*, nd.HoTen, nd.Email from PHIEU_MUON_TRA pmt join NGUOI_DUNG nd on nd.ID = pmt.MaDocGia where pmt.TinhTrang = 1 and pmt.MaTheLoaiPhieu = 1`
  return db.load(sql)
}

exports.getTicketDetail = function (ticketID) {
  var sql = `select * from PHIEU_MUON_TRA where ID = ${ticketID}`
  console.log(sql)
  return db.load(sql)
}

exports.getAllTicketIDsActive = function() {
  var sql = `select * from PHIEU_MUON_TRA where TinhTrang = 1 and MaTheLoaiPhieu = 1`
  return db.load(sql)
}

exports.getQuantityBorrowed = function(ticketID, user_id) {
  var sql = `select sum(ct.SoLuong) as TongSachDangMuon from PHIEU_MUON_TRA p join CHI_TIET_MUON_SACH ct on p.ID = ct.MaPhieu
              where p.MaDocGia = ${user_id} and p.ID = ${ticketID} and p.MaTheLoaiPhieu = 1 and p.TinhTrang = 1`
  return db.load(sql)
}

exports.checkExistedBook = function(ticketID, user_id, bookList) {
  var sql = `select ct.MaSach from PHIEU_MUON_TRA p join CHI_TIET_MUON_SACH ct on p.ID = ct.MaPhieu
              where p.MaDocGia = ${user_id} and p.ID = ${ticketID} and p.MaTheLoaiPhieu = 1 and p.TinhTrang = 1`
  return new Promise((resolve, reject) => {
    var foundNotExistedBookInTicket = false
    db.load(sql).then(value => {
      var newVal = value.map(v => v.MaSach)
      console.log('list', newVal)
      for (var b of bookList) {
        if (!newVal.includes(b.ID)) {
          foundNotExistedBookInTicket = true
          break
        }
      }
      resolve(foundNotExistedBookInTicket)
    }).catch(err => {
      reject(err)
    })
  })
}

exports.updateBorrowDetails = function(bookList, ticketID) {
  var results = []
  var sql = `select * from CHI_TIET_MUON_SACH where MaPhieu = ${ticketID}`

  return new Promise((resolve, reject) => {
    db.load(sql).then(rows => {
      for (var r of rows) {
        for (var b of bookList) {
          if (r.MaSach == b.ID) {
            var query = `update CHI_TIET_MUON_SACH set SoLuong = ${b.SoLuongMuon} where MaSach = ${b.ID} and MaPhieu = ${r.MaPhieu}`
            db.write(query).then(value => {
              results.push(b.ID)
            })
            if (b.SoLuongMuon == r.SoLuong) {
              var query2 = `update CHI_TIET_MUON_SACH set DaTra = 1 where MaSach = ${b.ID} and MaPhieu = ${r.MaPhieu}`
              db.write(query2)
            }
          }
        }
      }
      resolve(b.ID)
    }).catch(err => {
      reject(err)
    })
  })
}

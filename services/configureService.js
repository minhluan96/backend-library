var db = require('../utils/mysql-provider');

exports.getConfigOfUser = function() {
  var sql = `select distinct ch.* from CHI_TIET_CAU_HINH_DOC_GIA ct join CAU_HINH ch
              on ct.MaCauHinh = ch.ID`
  console.log(sql)
  return db.load(sql)
}

exports.updateConfig = function (id, configureEntity) {
  var sql = `update CAU_HINH set GiaTri = '${configureEntity.GiaTri}', LoaiThuocTinh = '${configureEntity.LoaiThuocTinh}',
              TenThuocTinh = '${configureEntity.TenThuocTinh}', TenDayDu = N'${configureEntity.TenDayDu}',
              TinhTrang = ${configureEntity.TinhTrang} where ID = ${id}`
  return db.write(sql)
}

exports.deleteConfig = function(id) {
  var sql = `update CAU_HINH set TinhTrang = 0 where ID = ${id}`
  console.log(sql)
  return db.write(sql)
}

exports.getAllConfigs = function () {
  var sql = `select * from CAU_HINH`
  return db.load(sql)
}

var db = require('../utils/mysql-provider');

exports.loadAllBooks = function() {
  var sql = 'select s.*, tl.TenTheLoai from SACH s join THE_LOAI_SACH tl on s.MaTheLoai = tl.ID where TinhTrang = 1';
  console.log(sql)
  return db.load(sql);
}

exports.loadBooksPerPage = function(limit, offset) {
  var sql = `select s.*, tl.TenTheLoai from SACH s join THE_LOAI_SACH tl on s.MaTheLoai = tl.ID limit ${limit} offset ${offset}`;
  return db.load(sql);
}

exports.getTotalBooks = function() {
  var sql = 'select count(*) as total from SACH';
  return db.load(sql);
}

exports.createBook = function(book) {
  var sql = `insert into SACH(TenSach, MaTheLoai, TacGia, NamXuatBan, NhaXuatBan,
    NgayNhap, TinhTrang, HinhAnh, SoLuong, MoTa) values(N'${book.TenSach}', ${book.MaTheLoai}, N'${book.TacGia}', ${book.NamXuatBan}, N'${book.NhaXuatBan}',
    '${book.NgayNhap}', ${book.TinhTrang}, '${book.HinhAnh}', ${book.SoLuong}, N'${book.MoTa}')`
  console.log(sql)
  return db.write(sql)
}

exports.updateBook = function (book) {
  var sql = `update SACH set TenSach = N'${book.TenSach}', MaTheLoai = ${book.MaTheLoai},
            TacGia = N'${book.TacGia}', NamXuatBan = ${book.NamXuatBan}, NhaXuatBan = N'${book.NhaXuatBan}',
            NgayNhap = '${book.NgayNhap}', TinhTrang = ${book.TinhTrang}, HinhAnh = '${book.HinhAnh}',
            SoLuong = ${book.SoLuong}, MoTa = N'${book.MoTa}' where ID = ${book.ID}`
  console.log(sql)
  return db.write(sql)
}

exports.deleteBook = function (book) {
  var sql = `update SACH set TinhTrang = 0 where ID = ${book.ID}`
  console.log(sql)
  return db.write(sql)
}

exports.searchAll = function (term) {
  var sql = `select count(*) as total from SACH s join THE_LOAI_SACH tl on s.MaTheLoai = tl.ID
              where TenSach like '%${term}%' or s.TacGia like '%${term}%' or tl.TenTheLoai like '%${term}%'`
  return db.load(sql)
}

exports.searchBookByTerm = function (limit, offset, term) {
  var sql = `select s.*, tl.TenTheLoai from SACH s join THE_LOAI_SACH tl on s.MaTheLoai = tl.ID where s.TenSach like '%${term}%'
              or s.TacGia like '%${term}%' or tl.TenTheLoai like '%${term}%' limit ${limit} offset ${offset}`
  return db.load(sql)
}

exports.updateQuantityBook = function (id, quantity) {
  var sql = `update SACH set SoLuong = ${quantity} where ID = ${id}`
  console.log(sql)
  return db.write(sql)
}

exports.getReportOfBooks = function () {
  var sql = `select tl.TenTheLoai, count(*) as total, s.MaTheLoai from SACH s join the_loai_sach tl on s.MaTheLoai = tl.ID group by s.MaTheLoai order by s.MaTheLoai asc`
  return db.load(sql)
}

exports.loadBorrowingBooks = function() {
  var sql = `select sum(ct.SoLuong) as SoLuongDangMuon, s.ID, s.TenSach, s.SoLuong as SoLuongTonKho, s.TacGia, s.NgayNhap, s.TinhTrang
              from SACH s join chi_tiet_muon_sach ct on ct.MaSach = s.ID join phieu_muon_tra pmt on pmt.ID = ct.MaPhieu
              where pmt.TinhTrang = 1 and pmt.MaTheLoaiPhieu = 1 group by s.ID`
  return db.load(sql)
}

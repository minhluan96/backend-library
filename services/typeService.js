var db = require('../utils/mysql-provider');

exports.getBookTypes = function () {
  var sql = `select * from THE_LOAI_SACH`;
  return db.load(sql);
}

exports.getMemberCardTypes = function () {
  var sql = `select * from THE_LOAI_THE_DOC_GIA`
  return db.load(sql)
}

exports.getTicketTypes = function () {
  var sql = `select * from THE_LOAI_PHIEU`
  return db.load(sql)
}

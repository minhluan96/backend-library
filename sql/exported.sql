CREATE DATABASE  IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library`;
-- MySQL dump 10.13  Distrib 5.7.24, for osx10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: library
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `NGUOI_DUNG`
--

DROP TABLE IF EXISTS `NGUOI_DUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NGUOI_DUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HoTen` varchar(200) DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `TinhTrang` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NGUOI_DUNG`
--

LOCK TABLES `NGUOI_DUNG` WRITE;
/*!40000 ALTER TABLE `NGUOI_DUNG` DISABLE KEYS */;
INSERT INTO `NGUOI_DUNG` VALUES (1,'Minh Luan','1996-12-12','luan@gmail.com',1);
/*!40000 ALTER TABLE `NGUOI_DUNG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PHIEU_MUON_TRA`
--

DROP TABLE IF EXISTS `PHIEU_MUON_TRA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHIEU_MUON_TRA` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MaDocGia` int(11) DEFAULT NULL,
  `NgayLapPhieu` date DEFAULT NULL,
  `TinhTrang` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_PMT_DG_idx` (`MaDocGia`),
  CONSTRAINT `FK_PMT_DG` FOREIGN KEY (`MaDocGia`) REFERENCES `NGUOI_DUNG` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PHIEU_MUON_TRA`
--

LOCK TABLES `PHIEU_MUON_TRA` WRITE;
/*!40000 ALTER TABLE `PHIEU_MUON_TRA` DISABLE KEYS */;
/*!40000 ALTER TABLE `PHIEU_MUON_TRA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUAN_LY`
--

DROP TABLE IF EXISTS `QUAN_LY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUAN_LY` (
  `ID` int(11) NOT NULL,
  `TenTaiKhoan` varchar(100) DEFAULT NULL,
  `MatKhau` varchar(200) DEFAULT NULL,
  `RefreshToken` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK_QL_ND` FOREIGN KEY (`ID`) REFERENCES `NGUOI_DUNG` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUAN_LY`
--

LOCK TABLES `QUAN_LY` WRITE;
/*!40000 ALTER TABLE `QUAN_LY` DISABLE KEYS */;
INSERT INTO `QUAN_LY` VALUES (1,'minhluan','6040247f302e4eb494bdfbbccd885a7f','487f7b22f68312d2c1bbc93b1aea445b');
/*!40000 ALTER TABLE `QUAN_LY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SACH`
--

DROP TABLE IF EXISTS `SACH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SACH` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TenSach` varchar(200) DEFAULT NULL,
  `MaTheLoai` int(11) DEFAULT NULL,
  `TacGia` varchar(200) DEFAULT NULL,
  `NamXuatBan` year(4) DEFAULT NULL,
  `NhaXuatBan` varchar(100) DEFAULT NULL,
  `NgayNhap` date DEFAULT NULL,
  `TinhTrang` tinyint(4) DEFAULT NULL,
  `HinhAnh` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_S_TLS_idx` (`MaTheLoai`),
  CONSTRAINT `FK_S_TLS` FOREIGN KEY (`MaTheLoai`) REFERENCES `THE_LOAI_SACH` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SACH`
--

LOCK TABLES `SACH` WRITE;
/*!40000 ALTER TABLE `SACH` DISABLE KEYS */;
INSERT INTO `SACH` VALUES (1,'Game of Thrones',2,'George R. R. Martin',2015,'HarperCollins UK','2018-10-28',1,'https://i.pinimg.com/564x/43/8b/f1/438bf109eb8c2209359385c7899039cd.jpg'),(2,'Tôi Thấy Hoa Vàng Trên Cỏ Xanh',1,'Nguyễn Nhật Ánh',2010,'NXB Trẻ','2018-10-28',1,'https://salt.tikicdn.com/cache/550x550/media/catalog/product/t/o/toi_thay_hoa_vang.jpg'),(3,'Ông Trăm Tuổi Trèo Qua Cửa Sổ Và Biến Mất',1,'Jonas Jonasson',2013,'NXB Trẻ','2018-10-29',1,'https://salt.tikicdn.com/cache/550x550/media/catalog/product/i/m/img448.u3059.d20170616.t101858.371205.jpg'),(4,'Khi Lỗi Thuộc Về Những Vì Sao',1,'John Green',2014,'NXB Trẻ','2018-10-28',1,'https://salt.tikicdn.com/cache/550x550/media/catalog/product/n/x/nxbtrestoryfull_02052014_030554_1.jpg');
/*!40000 ALTER TABLE `SACH` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `THE_DOC_GIA`
--

DROP TABLE IF EXISTS `THE_DOC_GIA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `THE_DOC_GIA` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NgayLapThe` date DEFAULT NULL,
  `DiemTichLuy` int(11) DEFAULT NULL,
  `ThoiHanSuDung` date DEFAULT NULL,
  `MaDocGia` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_TDG_ND_idx` (`MaDocGia`),
  CONSTRAINT `FK_TDG_ND` FOREIGN KEY (`MaDocGia`) REFERENCES `NGUOI_DUNG` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `THE_DOC_GIA`
--

LOCK TABLES `THE_DOC_GIA` WRITE;
/*!40000 ALTER TABLE `THE_DOC_GIA` DISABLE KEYS */;
/*!40000 ALTER TABLE `THE_DOC_GIA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `THE_LOAI_SACH`
--

DROP TABLE IF EXISTS `THE_LOAI_SACH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `THE_LOAI_SACH` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TenTheLoai` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `THE_LOAI_SACH`
--

LOCK TABLES `THE_LOAI_SACH` WRITE;
/*!40000 ALTER TABLE `THE_LOAI_SACH` DISABLE KEYS */;
INSERT INTO `THE_LOAI_SACH` VALUES (1,'Văn học'),(2,'Khoa học viễn tưỏng'),(3,'Giáo dục');
/*!40000 ALTER TABLE `THE_LOAI_SACH` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `THU_THU`
--

DROP TABLE IF EXISTS `THU_THU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `THU_THU` (
  `ID` int(11) NOT NULL,
  `TenTaiKhoan` varchar(100) DEFAULT NULL,
  `MatKhau` varchar(200) DEFAULT NULL,
  `RefreshToken` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK_TT_ND` FOREIGN KEY (`ID`) REFERENCES `NGUOI_DUNG` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `THU_THU`
--

LOCK TABLES `THU_THU` WRITE;
/*!40000 ALTER TABLE `THU_THU` DISABLE KEYS */;
/*!40000 ALTER TABLE `THU_THU` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-29 23:59:07

var express = require('express');
var configureService = require('../services/configureService');

var router = express.Router();

router.get('/', (req, res, next) => {
  configureService.getAllConfigs().then(rows => {
    res.statusCode = 200
    res.json(rows)
  }).catch(err => {
    res.statusCode = 500
    next(err)
  })
})

router.get('/user', (req, res, next) => {
  configureService.getConfigOfUser().then(rows => {
    res.statusCode = 200
    res.json(rows)
  }).catch(err => {
    res.statusCode = 500
    next(err)
  })
})

router.post('/:id', (req, res, next) => {
  var id = req.params.id
  configureService.updateConfig(id, req.body)
    .then(value => {
      res.statusCode = 201
      res.json(req.body)
    }).catch(err => {
      res.statusCode = 500
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  var id = req.params.id
  configureService.deleteConfig(id)
    .then(value => {
      res.statusCode = 201
      res.json(req.body)
    }).catch(err => {
      res.statusCode = 500
      next(err)
    })
})


module.exports = router;

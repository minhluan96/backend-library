var express = require('express');
var jwt = require('jsonwebtoken');
var userService = require('../services/userService');

var router = express.Router();

router.post('/', (req, res, next) => {
  var isCreateAccountForManager = req.query.isManager;
  userService.createAccountUser(req.body).then(value => {
    req.body.id = value.insertId;
    if (isCreateAccountForManager == 'true') {
      return userService.createAccountManager(req.body)
    }
    return userService.createNormalUser(req.body)
  })
  .then(value => {
    if (!req.query.isManager) {
      return userService.configureUser(req.body)
    }
  })
  .then(value => {
    res.statusCode = 201;
    res.json(req.body)
  })
  .catch(err => {
    res.statusCode = 500;
    next(err);
  })
})


router.post('/update/:id', (req, res, next) => {
  var isManager = req.query.isManager

  userService.updateUser(req.body).then(value => {
    if (isManager  == 'true') {
      return userService.updateAdminUser(req.body)
    }
    return userService.updateNormalUser(req.body)
  })
  .then(value => {
    res.statusCode = 201;
    res.json(req.body)
  })
  .catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.delete('/:id', (req, res, next) => {
  userService.deleteUser(req.body).then(value => {
    res.statusCode = 201;
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})


router.post('/login', (req, res, next) => {
  userService.login(req.body)
      .then(rows => {
        if (rows.length > 0) {
          var userEntity = rows[0];
          var payload = {
            user: userEntity
          }

          var accessToken = jwt.sign(payload, 'SECRET-LIBRARY', {
            expiresIn: '24h'
          });

          var refreshToken = userEntity.RefreshToken;
          res.json({
            auth: true,
            user: {
              id: userEntity.ID,
              username: userEntity.TenTaiKhoan,
              isQuanLy: userEntity.isQuanLy == 1
            },
            accessToken: accessToken,
            refreshToken: refreshToken
          })
        } else {
          res.json({
            auth: false
          })
        }
      }).catch(err => {
        res.statusCode = 500;
        next(err);
      })
})

router.get('/totalNormalID', (req, res, next) => {
  console.log('123')
  userService.getNormalIDs().then(values => {
    res.json(values)
  }).catch(err => {
    err.statusCode = 500
    next(err)
  })
})

router.get('/activeNormalUserID', (req, res, next) => {
  userService.getNormalUserIdsActive().then(values => {
    res.json(values)
  }).catch(err => {
    err.statusCode = 500
    next(err)
  })
})

router.get('/normal', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;

  var totalItems = 0
  var totalPages = 0

  userService.getTotalNormalUser().then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return userService.getNormalUserPerPage(perPage, offset)
  }).then(rows => {
    res.json({
      results: rows,
      perPage: rows.length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
})

router.get('/admin', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;

  var totalItems = 0
  var totalPages = 0

  userService.getTotalAdminUser().then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return userService.getAdminUserPerPage(perPage, offset)
  }).then(rows => {
    res.json({
      results: rows,
      perPage: rows.length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
})

router.get('/total', (req, res, next) => {
  userService.loadAllUsers().then(rows => {
    totalUsers = rows[0].total
    res.json(totalUsers)
  }).catch(err =>{
    next(err)
  })
})

module.exports = router;

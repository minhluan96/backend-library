var express = require('express');
var memberCardService = require('../services/memberCardService');

var router = express.Router();

router.get('/', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;

  var totalItems = 0
  var totalPages = 0

  memberCardService.getTotalMemberCards().then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return memberCardService.getMemberCardsPerPage(perPage, offset)
  }).then(rows => {
    res.json({
      results: rows,
      perPage: rows.length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
})

router.post('/update/:id', (req, res, next) => {
  memberCardService.updateMemberCard(req.body).then(value => {
    res.statusCode = 201;
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.delete('/:id', (req, res, next) => {
  memberCardService.deleteMemberCard(req.body).then(value => {
    res.statusCode = 201;
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.post('/', (req, res, next) => {
  memberCardService.createMemberCard(req.body).then(value => {
    res.statusCode = 201;
    var id = value.insertId
    req.body.ID = id
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.get('/total', (req, res, next) => {
  memberCardService.getTotalMemberCards().then(value => {
    var totalCards = value[0].total
    res.json(totalCards)
  }).catch(err => {
    next(err)
  })
})


module.exports = router;

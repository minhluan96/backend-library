var express = require('express');
var ticketService = require('../services/ticketService');
var bookService = require('../services/bookService');
var configureService = require('../services/configureService');
var memberCardService = require('../services/memberCardService');
var moment = require('moment');

var router = express.Router();

var updateQuantityOfBook = function (books, type) {

  var theLoaiPhieu = type

  return new Promise((resolve, reject) => {
    for (var book of books) {
      var quantity = theLoaiPhieu == 1 ? book.SoLuong - book.SoLuongMuon : book.SoLuong + book.SoLuongMuonCu
      if (book.isNewAdded == true) {
        quantity = theLoaiPhieu == 1 ? book.SoLuong - book.SoLuongMuon : book.SoLuong
      }
      if (book.isDeletedFromTicket == true) {
        quantity = theLoaiPhieu == 1 ? book.SoLuong : quantity
      }
      book.SoLuong = quantity
      bookService.updateQuantityBook(book.ID, quantity).then(value => {

      }).catch(err => {
        reject(err)
      })
    }
    resolve(books)
  })
}

router.get('/search', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;
  var term = req.query.term || ''

  var totalItems = 0
  var totalPages = 0
  ticketService.searchAll(term).then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return ticketService.searchTicketByTerm(perPage, offset, term)
  }).then(rows => {
    var length = rows instanceof Array ? rows.length : 1
    res.json({
      results: rows,
      perPage: length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
})

router.get('/', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;

  var totalItems = 0
  var totalPages = 0

  ticketService.loadAllTickets().then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return ticketService.loadTicketsPerPage(perPage, offset)
  }).then(rows => {
    res.json({
      results: rows,
      perPage: rows.length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
});

router.post('/validate', (req, res, next) => {
  var ticketReq = req.body
  var theLoaiPhieu = ticketReq.MaTheLoaiPhieu
  var user_id = ticketReq.MaDocGia
  var maxBorrowDay = 0
  var maxBorrowBooks = 0
  configureService.getAllConfigs().then(value => {
    var configs = value
    maxBorrowDay = configs.find(function(conf) { return conf.TenThuocTinh === 'SoLuongNgayMuonToiDa' })
    maxBorrowBooks = configs.find(function(conf) { return conf.TenThuocTinh === 'SoLuongSachMuonToiDa' })

    maxBorrowDay = maxBorrowDay == null ? 0 : maxBorrowDay.GiaTri
    maxBorrowBooks = maxBorrowBooks == null ? 0 : maxBorrowBooks.GiaTri
    return ticketService.getTotalActiveBorrowBooks(user_id)
  }).then(value => {
    console.log('total books: ', value)
    console.log('The Loai phieu', theLoaiPhieu)
    if (theLoaiPhieu == 1) {
      var totalAttendToBorrow = 0
      var totalBooks = req.body.bookList
      for (var b of totalBooks) {
        totalAttendToBorrow = totalAttendToBorrow + b.SoLuongMuon
      }
      console.log('Tong sach se muon', value[0].TongSachDangMuon + totalAttendToBorrow)
      console.log(value[0].TongSachDangMuon >= maxBorrowBooks || totalAttendToBorrow > maxBorrowBooks || value[0].TongSachDangMuon + totalAttendToBorrow >= maxBorrowBooks)
      if (value[0].TongSachDangMuon >= maxBorrowBooks || totalAttendToBorrow > maxBorrowBooks || value[0].TongSachDangMuon + totalAttendToBorrow > maxBorrowBooks) {
        res.statusCode = 403
        console.log('show error here')
        var err = new Error(`Số lượng sách đang mượn vượt quá mức quy định: ${maxBorrowBooks} sách`);
        return next(err);
      }
    }
    return ticketService.getFirstTicketBorrowBook(user_id)
  }).then(value => {
    if (value[0] != null && theLoaiPhieu == 1) {
      var ngayLapPhieu = moment(value[0].NgayLapPhieu)
      var ngayHienTai = moment()
      console.log('date', ngayHienTai.diff(ngayLapPhieu, 'days'))
      if (ngayHienTai.diff(ngayLapPhieu, 'days') > maxBorrowDay ) {
        res.statusCode = 403
        var err = new Error(`Số ngày mượn sách chưa trả đang vượt quá mức quy định: ${maxBorrowDay} sách`);
        return next(err);
      }
    }
    res.statusCode = 200
    res.json({ message: 'ok' })
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

var calculateMemberPoint = function (ngayLapPhieu) {
  var maxBorrowDay = 0
  var maxBorrowBooks = 0

  configureService.getAllConfigs().then(value => {
    var configs = value
    maxBorrowDay = configs.find(function(conf) { return conf.TenThuocTinh === 'SoLuongNgayMuonToiDa' })
    maxBorrowBooks = configs.find(function(conf) { return conf.TenThuocTinh === 'SoLuongSachMuonToiDa' })

    maxBorrowDay = maxBorrowDay == null ? 0 : maxBorrowDay.GiaTri
    maxBorrowBooks = maxBorrowBooks == null ? 0 : maxBorrowBooks.GiaTri

    var ngayHienTai = moment()
    console.log('logs', ngayHienTai.diff(moment(ngayLapPhieu), 'days'))
    if (ngayHienTai.diff(moment(ngayLapPhieu, 'DD/MM/YYYY'), 'days') <= maxBorrowDay) {
      return true
    }
    return false
  })
}

router.post('/', (req, res, next) => {
  var ticketReq = req.body
  console.log(ticketReq)
  var theLoaiPhieu = ticketReq.MaTheLoaiPhieu
  var user_id = ticketReq.MaDocGia
  var maxBorrowDay = 0
  var maxBorrowBooks = 0
  var tongSoSachTra = 0
  ticketService.createTicket(ticketReq).then(value => {
    var id = value.insertId
    req.body.ID = id
    var books = ticketReq.bookList

    return new Promise((resolve, reject) => {
      for (var book of ticketReq.bookList) {
        var quantity = theLoaiPhieu == 1 ? book.SoLuong - book.SoLuongMuon : book.SoLuong + book.SoLuongMuon
        tongSoSachTra = tongSoSachTra + book.SoLuongMuon
        bookService.updateQuantityBook(book.ID, quantity).catch(err => {
          reject(err)
        })
      }
      resolve(books)
    })
  }).then(value => {
    if (theLoaiPhieu == 2) {
      return ticketService.checkExistedBook(ticketReq.MaPhieuDaMuon, user_id, ticketReq.bookList)
    }
  }).then(value => {
    console.log('COMPARE', value)
    if (theLoaiPhieu == 2) {
      if (value) {
        res.statusCode = 403
        var err = new Error(`Tồn tại sách không có trong phiếu mượn. Vui lòng kiểm tra lại`);
        throw err
      }
      return ticketService.getQuantityBorrowed(ticketReq.MaPhieuDaMuon, user_id)
    }
  }).then(value => {
    if (theLoaiPhieu == 2) {
      var tongSachDangMuon = value[0].TongSachDangMuon
      console.log('TONG SO SACH DANG MUON', tongSachDangMuon)
      console.log('TONG SO SACH TRA', tongSoSachTra)
      console.log('compare', tongSachDangMuon == tongSoSachTra)
      if (tongSoSachTra > tongSachDangMuon) {
        res.statusCode = 403
        var err = new Error(`Số sách đang mượn là ${tongSachDangMuon} không hợp lý với số sách trả là ${tongSoSachTra}`);
        throw err
      }
      if (tongSachDangMuon == tongSoSachTra) {
        return ticketService.updateDetailStatus(ticketReq.MaPhieuDaMuon)
      } else {
        return ticketService.updateBorrowDetails(ticketReq.bookList, ticketReq.MaPhieuDaMuon)
      }
    }
  }).then(value => {
    return configureService.getAllConfigs()
  }).then(value => {
    var configs = value
    maxBorrowDay = configs.find(function(conf) { return conf.TenThuocTinh === 'SoLuongNgayMuonToiDa' })
    maxBorrowBooks = configs.find(function(conf) { return conf.TenThuocTinh === 'SoLuongSachMuonToiDa' })

    maxBorrowDay = maxBorrowDay == null ? 0 : maxBorrowDay.GiaTri
    maxBorrowBooks = maxBorrowBooks == null ? 0 : maxBorrowBooks.GiaTri

    var ngayHienTai = moment()
    console.log('gia tri', ngayHienTai.diff(moment(ticketReq.ngayLapPhieu), 'days') <= maxBorrowDay && ticketReq.MaTheLoaiPhieu == 2)
    if (ngayHienTai.diff(moment(ticketReq.ngayLapPhieu), 'days') <= maxBorrowDay && ticketReq.MaTheLoaiPhieu == 2) {
      console.log('update here')
      return memberCardService.updateMemberPoint(ticketReq.bookList.length, ticketReq.MaDocGia)
    } else if (ngayHienTai.diff(moment(ticketReq.ngayLapPhieu), 'days') > maxBorrowDay && ticketReq.MaTheLoaiPhieu == 2) {
      return memberCardService.decreaseMemberPoint(ticketReq.bookList.length, ticketReq.MaDocGia)
    }
  }).then(value => {
    return ticketService.createDetailBorrowBooks(ticketReq)
  }).then(value => {
    return bookService.loadAllBooks()
  }).then(value => {
    res.statusCode = 201
    res.json(value)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.get('/:id/books', (req, res, next) => {
  var id = req.params.id
  ticketService.getDetailBookOfTicket(id).then(rows => {
    res.statusCode = 201;
    res.json(rows)
  }).catch(err => {
    res.statusCode = 500;
    next(err)
  })
})

router.get('/details/:id', (req, res, next) => {
  var id = req.params.id
  ticketService.getTicketDetail(id).then(value => {
    res.statusCode = 201
    res.json(value[0])
  }).catch(err => {
    res.statusCode = 500
    next(err)
  })
})

router.post('/:id', (req, res, next) => {
  var id = req.params.id
  var ticketReq = req.body
  var books = ticketReq.bookList
  ticketService.updateBorrowTicket(ticketReq).then(value => {

    var theLoaiPhieu = 2

    return updateQuantityOfBook(books, theLoaiPhieu).then(value => {
      books = value
    })
  }).then(value => {
    return ticketService.deleteAllBorrowBooks(req.params.id)
  }) .then(value => {
    return updateQuantityOfBook(books, ticketReq.MaTheLoaiPhieu)
  }).then(value => {
    if (books.length != 0) {
      return ticketService.createDetailBorrowBooks(ticketReq)
    }
  }).then(value => {
    if (ticketReq.MaTheLoaiPhieu == 2) {

      return ticketService.deleteTicket(ticketReq.MaPhieuDaMuon)
    }
  }).then(value => {
    return bookService.loadAllBooks()
  }).then(rows => {
    res.statusCode = 201
    res.json(rows)
  }).catch(err => {
    res.statusCode = 500
    next(err)
  })
})

router.delete('/:id', (req, res, next) => {
  var id = req.params.id
  var ticketReq = req.body
  var books = ticketReq.bookList
  ticketService.deleteTicket(id).then(value => {
    return updateQuantityOfBook(books, 2)
  }).then(value => {
    return ticketService.deleteAllBorrowBooks(id)
  }).then(value => {
    return bookService.loadAllBooks()
  }).then(rows => {
    res.statusCode = 201
    res.json(rows)
  }).catch(err => {
    res.statusCode = 500
    next(err)
  })
})

router.get('/total', (req, res, next) => {
  ticketService.loadAllTickets().then(rows => {
    res.statusCode = 200
    totalTickets = rows[0].total
    res.json(totalTickets)
  }).catch(err => {
    next(err)
  })
})

router.get('/report', (req, res, next) => {
  var report = {}
  ticketService.getAllDateOfTickets().then(values => {
    report.allDates = values
    return ticketService.getReportOfTickets()
  }).then(values => {
    res.statusCode = 200
    report.report = values
    res.json(report)
  }).catch(err => {
    next(err)
  })
})

router.get('/borrowTickets', (req, res, next) => {
  ticketService.loadBorrowTicket().then(values => {
    res.statusCode = 200
    res.json(values)
  }).catch(err => {
    next(err)
  })
})

module.exports = router;

var express = require('express');
var bookService = require('../services/bookService');
var typeService = require('../services/typeService');
var multer = require('multer')
var upload = multer({ dest: 'uploads/' })

var router = express.Router();

router.get('/', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;

  var totalItems = 0
  var totalPages = 0

  bookService.getTotalBooks().then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return bookService.loadBooksPerPage(perPage, offset)
  }).then(rows => {
    res.json({
      results: rows,
      perPage: rows.length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
});

router.get('/all', (req, res, next) => {
  bookService.loadAllBooks().then(value => {
    res.statusCode = 201;
    res.json(value)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.post('/', (req, res, next) => {
  bookService.createBook(req.body).then(value => {
    res.statusCode = 201;
    var id = value.insertId
    req.body.ID = id
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.post('/:id', (req, res, next) => {
  bookService.updateBook(req.body).then(value => {
    res.statusCode = 201;
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.delete('/:id', (req, res, next) => {
  bookService.deleteBook(req.body).then(value => {
    res.statusCode = 201;
    res.json(req.body)
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

router.get('/search', (req, res, next) => {
  var page = req.query.page || 1;
  var perPage = req.query.per_page || 2;
  var offset = (page - 1) * perPage;
  var term = req.query.term || ''

  var totalItems = 0
  var totalPages = 0

  bookService.searchAll(term).then(rows => {
    totalItems = rows[0].total
    totalPages = Math.ceil(totalItems / perPage)
    return bookService.searchBookByTerm(perPage, offset, term)
  }).then(rows => {
    var length = rows instanceof Array ? rows.length : 1
    res.json({
      results: rows,
      perPage: length,
      totalPages: totalPages
    })
  }).catch(error => {
    next(error);
  })
})

router.get('/total', (req, res, next) => {
  bookService.getTotalBooks().then(value => {
    totalItems = value[0].total
    res.json(totalItems)
  }).catch(err => {
    next(err)
  })
})

router.get('/report', (req, res, next) => {
  var report = {}
  bookService.getTotalBooks().then(value => {
    report.totalBooks = value[0].total
    return bookService.getReportOfBooks()
  }).then(value => {
    res.statusCode = 200
    report.report = value
    res.json(report)
  }).catch(err => {
    next(err)
  })
})

router.get('/reportStatusBooks', (req, res, next) => {
  bookService.loadBorrowingBooks().then(values => {
    res.statusCode = 200
    res.json(values)
  }).catch(err => {
    next(err)
  })
})

module.exports = router;

var express = require('express');
var typeService = require('../services/typeService');

var router = express.Router();

router.get('/bookTypes', (req, res, next) => {
  typeService.getBookTypes().then(rows => {
    res.statusCode = 201;
    res.json(rows);
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
});

router.get('/memberCardTypes', (req, res, next) => {
  typeService.getMemberCardTypes().then(rows => {
    res.statusCode = 201;
    res.json(rows);
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
});

router.get('/ticketTypes', (req, res, next) => {
  typeService.getTicketTypes().then(rows => {
    res.statusCode = 201;
    res.json(rows);
  }).catch(err => {
    res.statusCode = 500;
    next(err);
  })
})

module.exports = router;
